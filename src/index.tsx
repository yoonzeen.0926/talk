import React from "react";
import ReactDOM from "react-dom/client";
import reportWebVitals from "./reportWebVitals";
import './assets/css/reset.scss'
import './assets/css/common.scss'
import { BrowserRouter, Route, Routes } from "react-router-dom";
import ListPage from "./pages/List";
import RoomPage from "./pages/talk/Room";
import CreatePage from "./pages/talk/Create";
import JoinedRoomPage from "./pages/talk/Joined";
import MorePage from 'pages/talk/More';
import ReportPage from 'pages/Report';
import ProfilePage from 'pages/Profile';

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);

root.render(
  <>
    <BrowserRouter>
      <Routes>
        <Route path="/" index element={<ListPage />} />
        <Route path="/talk">
          <Route path="room/:id" element={<RoomPage />} />
          <Route path="joined" element={<JoinedRoomPage />} />
          <Route path="new" element={<CreatePage />} />
          <Route path="more" element={<MorePage />} />
        </Route>
        <Route path="/profile" element={<ProfilePage />} />
        <Route path="/report" element={<ReportPage />} />
      </Routes>
    </BrowserRouter>
  </>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
