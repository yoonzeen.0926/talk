// 천, 만 단위 붙이기
export const convertUnit = (num:number) => {
    const str = num.toString();
    if (str.length <= 3) {
        return num;
    } else if (str.length === 4) {
        const temp = num / 1000;
        return Math.floor(temp * 10) / 10 + '천';
    } else if (str.length >= 5) {
        const temp = num / 10000;
        return Math.floor(temp * 10) / 10 + '만';
    } else {
        return;
    }
}
