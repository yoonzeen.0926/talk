// 카운트다운 시간 계산
export const calculateCountDown = (date :string) => {
    const now = new Date();
    const deleteDate = new Date(date);
    let diffTime = deleteDate.getTime() - now.getTime();
    let diffHours = Math.floor(diffTime / (1000 * 60 * 60));
    let diffMin = Math.floor(diffTime / (1000 * 60) % 60);
    let diffSec = Math.floor(diffTime / (1000) % 60);
    return convertTime(diffHours, diffMin, diffSec);
}

// 한자리 수 무조건 두자리 수로 만들기
export const numberToTwoLetters = (num:number) => {
    return num < 10 ? '0' + num : num;
}

// HH:MM:SS 변환
export const convertTime = (h:number, m:number, s:number) => {
    if (h <= 0 && m <= 0 && s <= 0) return '00:00:00';
    return numberToTwoLetters(h) + ':' + numberToTwoLetters(m) + ':' + numberToTwoLetters(s);
}

// 데드라인까지 몇 퍼센트
export const convertPercent = (deleteDate:string) => {
    const now = new Date();
    const timestampOfNow = now.getTime();
    const dDate = new Date(deleteDate);
    const beforeDDate = new Date(deleteDate);
    beforeDDate.setDate(dDate.getDate() - 1);
    const timestampOfDDate = dDate.getTime();
    const timestampOfBeforeDDate = beforeDDate.getTime();
    return Math.floor((timestampOfNow - timestampOfBeforeDDate) / (timestampOfDDate - timestampOfBeforeDDate) * 100) + '%';
}

// 60초 여부
export const isLeft60Secs = (deleteDate:string) => {
    const now = new Date();
    now.setDate(now.getMinutes() + 1);
    const dDate = new Date(deleteDate);
    const timestampOfDDate = dDate.getTime();
    return now.getTime() === timestampOfDDate ? true : false;
}

// 채팅방 시각 표기
export const convertTimeInRoom = (date:string) => {
    const d = new Date(date);
    const isUpTwelve = d.getHours() >= 12 ? '오후' : '오전';
    const monthOfd = d.getMonth() + 1;
    const dateOfd = d.getDate();
    const hoursOfd = d.getHours() > 12 ? d.getHours() - 12 : d.getHours();
    const minsOfd = d.getMinutes();
    return monthOfd + '. ' + dateOfd + ' ' + isUpTwelve + ' ' + hoursOfd + ':' + minsOfd;
}