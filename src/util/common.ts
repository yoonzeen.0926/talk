export const colorArr = ['lightblue', 'orange', 'yellow', 'blue', 'cream', 'lightpink'];
// 컬러 랜덤으로 리턴하기
export const randomColor = (num:number) => {
    const idx = Math.floor(Math.random() * colorArr.length)
    return colorArr[idx];
}
