import api from './index'

// 대주제 카테고리
export const categoryApi = () => {
    const response = api.get('/v1/category');
    return response;
}