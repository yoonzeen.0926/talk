import api from './index'

// 채팅방 조회
export const talkRoomApi = (id:string) => {
    const response = api.get(`/v1/room?roomId=${id}`);
    return response;
}

// 소주제 리스트
export const talkRoomsApi = (categoryCode:string) => {
    const response = api.get(`/v1/rank?category=${categoryCode}&page=0&size=4&sort=rank%2CASC`);
    return response;
}