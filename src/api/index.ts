import axios from 'axios'

const api = axios.create({
    baseURL: process.env.REACT_APP_API_HOST,
    headers: {
        Accept: 'application/json;charset=UTF-8'
    }
});
export default api;