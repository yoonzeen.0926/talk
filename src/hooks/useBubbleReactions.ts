import { useState } from 'react';

const useBubbleReactions = (flag:boolean) => {
    const [openReactions, setOpenReactions] = useState<boolean>(flag);

    if (flag) {
        setOpenReactions(true);
    } else {
        setOpenReactions(false);
    }
  
    return openReactions;
}
export default useBubbleReactions