
const useBodyScroll = (flag:boolean) => {
    if (document.querySelector('body')) {
        const body = document.querySelector('body') as HTMLBodyElement;
        if (flag) {
            body.setAttribute('style', 'overflow:hidden');
        } else {
            body.style.removeProperty('overflow');
        }
    }
}
export default useBodyScroll