// vh - 화면 크기 변경 대응
const useConvertVh = () => {
    const doc = document.documentElement;
    const setDoc = () => doc.style.setProperty('--vh', `${window.innerHeight * 0.01}px`);

    setDoc();
    setTimeout(setDoc, 100); // ios 바로 변경 안되는 이슈 대응
    
    window.addEventListener('resize', () => {
        setDoc();
    });
    return;
}
export default useConvertVh;