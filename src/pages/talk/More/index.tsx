import Header from 'components/Header/Header';
import ShapeOfRoom from 'components/Room/Shape/ShapeOfRoom';
import BoxTab from 'components/common/Tab';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import './index.scss'

const MorePage = () => {
    const temp = [
        {
            krName:'인기순',
            enName:'popular',
            id:0
        },
        {
            krName: '최신순',
            enName: 'recent',
            id:1
        }
    ]
    const navigate = useNavigate();
    
    // 방 진입
    const enterTalkRoom = (id: string) => {
        navigate(`/talk/room/${id}`);
        window.scrollTo(0, 0);
    };
    return (
        <div className='wrapper depth'>
            <Header isDepth={true} title={'10대 이야기'} />
            <div className='sorting_wrap'>
                <BoxTab tabs={temp} position='right' />
            </div>
            <div className='list_of_category_wrap'>
                {/* map */}
                <div className='list_of_category'>
                    <div className='shapes'>
                        <ShapeOfRoom onclick={() => enterTalkRoom('1')} roomName={'따라 말하기 컨셉방'} isWide={true} isSquare={true} deleteDate={'2024-07-05 00:00:00'} messageCount={8787}  />
                    </div>
                </div>
                <div className='list_of_category'>
                    <div className='shapes'>
                        <ShapeOfRoom onclick={() => enterTalkRoom('1')} roomName={'따라 말하기 컨셉방'} isWide={true} isSquare={true} deleteDate={'2024-07-05 00:00:00'} messageCount={8787}  />
                    </div>
                </div>
                <div className='list_of_category'>
                    <div className='shapes'>
                        <ShapeOfRoom onclick={() => enterTalkRoom('1')} roomName={'따라 말하기 컨셉방'} isWide={true} isSquare={true} deleteDate={'2024-07-05 00:00:00'} messageCount={8787}  />
                    </div>
                </div>

            </div>
        </div>
    );
};

export default MorePage;