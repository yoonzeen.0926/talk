import { categoryApi } from 'api/section';
import Header from 'components/Header/Header';
import { BasicButton } from 'components/common/Button';
import Category, { categoryProps } from 'components/common/Category';
import { InputBtnGroup, InputTextGroup } from 'components/common/Input';
import Title from 'components/common/Title';
import useConvertVh from 'hooks/useConvertVh';
import { categoriesData } from 'pages/List';
import React, { useEffect, useState } from 'react';
import './index.scss'
import { useNavigate } from 'react-router-dom';

const CreatePage = () => {
    const [categories, setCategories] = useState<categoryProps[]>(categoriesData);
    const [readyToSubmit, setReadyToSubmit] = useState<boolean>(false);
    const [errOfValueOfName, setErrOfValueOfName] = useState(false);
    const navigate = useNavigate();
    const reg = /^[ㄱ-ㅎㅏ-ㅣ가-힣a-zA-Z0-9!?.]+$/; // 펑톡방 이름 정규식
    const [values, setValues] = useState({
        category : {
            categoryCode: '',
            categoryName: '',
            order:0,
            useFlag : ''
        },
        name: '',
        isImage: false
    })

    useConvertVh();

    const handleCategory = (category:categoryProps) => {
        console.log(values.category)
        setValues({
            ...values,
            category
        })
    }

    // 이미지 펑톡방 유무
    const handleCheck = (e:React.ChangeEvent<HTMLInputElement>) => {
        const target = e.target;
        const val = target.value;
        setValues({
            ...values, 
            isImage: val === 'no' ? true : false
        });
    }

    // 펑톡방 이름 input
    const handleInput = (e:React.ChangeEvent<HTMLInputElement>) => {
        const target = e.target;
        const val = target.value;
        setValues({
            ...values, 
            name : val
        });
        if (val.length === 0) {
            setErrOfValueOfName(false);
            return;
        }

        // check error
        if (reg.test(val)) {
            setErrOfValueOfName(false);
        } else {
            setErrOfValueOfName(true);
        }
    }

    // 완료 버튼
    const handleSubmit = () => {
        navigate(`/talk/room/1`);
    };

    // input 값 지우기
    const resetValueOfName = () => {
        setValues({
            ...values,
            name : ''
        });
        setErrOfValueOfName(false);
    }

    
    // 대주제 카테고리 API
    const loadCategories = async () => {
        try {
            const res = await categoryApi();
            setCategories(res.data.data.categories);
        } catch(err) {
            console.log(err)
        }
    }

    useEffect(() => {
        loadCategories();
    }, []);

    useEffect(() => {
        // 완료버튼 활성화
        if (values.category.order !== 0 && values.name !== '' && !errOfValueOfName) {
            setReadyToSubmit(true);
        } else {
            setReadyToSubmit(false);
        }
    }, [values, errOfValueOfName]);

    return (
        <div className='wrapper depth create'>
            <Header title='' backType='close' isDepth={true} btns={true}>
                <BasicButton type={'text'} text={'완료'} onclick={handleSubmit} isDisabled={!readyToSubmit} />
            </Header>
            <div className='section cate'>
                <Title isSub={true} text={'원하는 대주제를 선택하세요.'} />
                <div className='content'>
                {
                    categories && <Category categories={categories || categoriesData} selectedIdx={values.category.order !== 0 ? values.category.order : undefined} onclick={handleCategory}  /> 
                }
                </div>
            </div>
            <div className='section name'>
                <Title isSub={true} text={'펑톡방 이름을 만들어보세요.'} />
                <div className='content'>
                    <InputTextGroup value={values.name} onchange={handleInput} max={10} isError={errOfValueOfName} 
                    reset={resetValueOfName} 
                    errorText={'조건에 맞지 않는 펑톡방 이름입니다.'}
                    help={<>한글, 영문 대소문자, 숫자, 띄어쓰기를 포함하여 최대 10글자까지<br/>
                입력 가능합니다. (특수문자 사용 불가)</>}
                    />
                </div>
            </div>
            <div className='section image'>
                <div className='title_wrap'>
                    <Title isSub={true} text={'이미지 펑톡방 설정'} />      
                    <div className='content'>          
                        <label>
                            <span className='input_btn_area'>
                                <InputBtnGroup type='checkbox' name='isImageRoom' classNames='toggle' value={values.isImage ? 'ok' : 'no'} onchange={handleCheck} checked={values.isImage} />
                            </span>
                        </label>
                    </div>    
                </div>
                <div className='help_text'>
                    펑톡방에서 이미지 전송만 가능합니다.<br/>
                    설정 후, 변경 불가합니다.
                </div>
            </div>
        </div>
    );
};

export default CreatePage;