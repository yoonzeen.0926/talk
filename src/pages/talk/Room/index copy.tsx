import React, { useRef, useState } from "react";
import { useParams } from "react-router-dom";
// import SockJS from "sockjs-client";

// style
import "./index.scss";
import PungPortal from "components/Pung/Portal";
import ListPage from "pages/List";
// import TalkInput from "components/TalkInput/TalkInput";

export type bubbleInfo = {
  name: string;
  message: string;
};

const TalkPage = ({ depth = true }: any) => {
  const [isDepth, setIsDepth] = useState<boolean>(depth);
  const param = useParams();
  const talkId = param ? param.id : null;
  const [live, setLive] = useState<boolean>(false);
  const [message, setMessage] = useState("");
  const [talk, setTalk] = useState<bubbleInfo[]>([]);
  //   const [sockjs, setSockjs] = useState<any>();

  const [isPung, setIsPung] = useState<boolean>(false);
  const [isPungOn, setIsPungOn] = useState<boolean>(false);

  const inputRef = useRef<HTMLInputElement>(null);

  // connect server
  const onClickConnectBtn = () => {
    // const sock = new SockJS("http://localhost:9999/echo");
    // sock.onmessage = function (e) {
    //   setReceivedData(e.data);
    //   console.log(e);
    // };
    // setSockjs(sock);
    setTalk([...talk, { name: "testuser", message: "님이 입장하셨습니다." }]);
    setLive(true);
  };

  // disconnect server
  const onClickDisconnectBtn = () => {
    setLive(false);
  };

  // change value of the input
  const changeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    setMessage(e.target.value);
  };

  // render
  const renderTalk = () => {
    return talk.map(({ name, message }, idx) => (
      <div key={idx}>
        <>
          {name}: <>{message}</>
        </>
      </div>
    ));
  };

  // event of 'enter key'
  const onEnter = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.keyCode === 13) {
      sendMessage();
    }
  };

  // send
  const sendMessage = () => {
    if (message === "") return;
    setTalk([...talk, { name: "나", message: message }]);
    // sockjs.send(message);
    setMessage("");
  };

  // Effect pung
  const controlPung = () => {
    inputRef?.current?.blur();
    setIsDepth(false);
    setTimeout(() => {
      setIsPungOn(true);
      window.history.pushState("", "test", `/`);
    }, 3000);
  };

  // count down - ex) when click input
  const startCountDown = () => {
    setTimeout(() => {
      setIsPung(true);
      controlPung();
    }, 3000);
  };


  // useEffect(()=>{
  //     if(receivedData === '') return;
  //     setTalk(talk => [...talk, {name: "Server", message: receivedData + '(이)라고 말했습니다.'}])
  // },[receivedData])

  return (
    <div className='wrapper'>
      {
        // 목록 <-> 상세 페이지
        isDepth ? (
          <div className="room">
            <h3>톡방 - {talkId}</h3>
            {live ? (
              <>
                {/* talk */}
                <div>{renderTalk()}</div>

                {/* input */}
                <div>
                  {/* <TalkInput value={message} onKeyDown={onEnter} onChange={changeInput} onClick={startCountDown} inputRef={inputRef} /> */}
                  <button type="button" className="btn send" onClick={sendMessage}>
                    전송
                  </button>
                  <button type="button" className="btn disconnect" onClick={onClickDisconnectBtn}>연결 끊기</button>
                </div>
              </>
            ) : (
              <button type="button" className="btn connect" onClick={onClickConnectBtn}>연결</button>
            )}
          </div>
        ) : (
          <ListPage />
        )
      }
      {isPung && <PungPortal isOn={isPungOn}></PungPortal>}
    </div>
  );
};

export default TalkPage;
