import { talkRoomApi, talkRoomsApi } from 'api/Room';
import Header from 'components/Header/Header';
import RoomList, { roomProps } from 'components/Room/List/RoomList';
import React, { useCallback, useEffect, useRef, useState } from "react";
import {useParams } from "react-router-dom";
import Bubble from 'components/Room/Bubble/Bubble';
import './index.scss'
import TalkInput from 'components/Room/TalkInput/TalkInput';
import RoomInfo from 'components/Room/Info/RoomInfo';
import useConvertVh from 'hooks/useConvertVh';
import { ButtonWrapper, ExternalLink, YellowBorderButton, YellowButton } from 'components/common/Button';
import PannAgreeModalPortal from 'components/Modal/PannAgreeModalPortal';
import { InputBtnGroup } from 'components/common/Input';

export type bubbleInfo = {
  name: string;
  message: string;
};
export type talkInfoProps = {
  categoryCode : string;
  roomId: string;
  roomName: string;
  createFlag: string;
  messageCount: number;
  deleteDate: string;
}

const RoomPage = () => {
  // 이용약관 동의 관련 
  const [isPannAgreeCheckbox, setIsPannAgreeCheckbox] = useState<boolean>(false);
  const [isPannAgree, setIsPannAgree] = useState<boolean>(false);

  const [room, setRoom] = useState<roomProps>(); // 현재 방 정보 
  const [rooms, setRooms] = useState<roomProps[]>([]); // 소주제 리스트

  const [receivedNewMessage, setReceivedNewMessage] = useState<boolean>(true);
  const [isBtnBottom, setIsBtnBottom] = useState<boolean>(false);
  
  const [categoryCode, setCategoryCode] = useState<string>('');
  const [allOpenReactions, setAllOpenReactions] = useState<boolean>(true);
  const param = useParams();
  const roomId = param ? param.id : '';
  const talkWrapRef = useRef<HTMLDivElement>(null);
  const contentsAreaRef = useRef<HTMLDivElement>(null);

  useConvertVh();

  // 채팅방 조회 API
  const loadTalkRoom = useCallback(async() => {
      try {
        if (roomId) {
          const res = await talkRoomApi(roomId);
          setRoom(res.data.data);
          setCategoryCode(res.data.data.categoryCode);
        }
      } catch(err) {
        console.log(err);
      }
    
  }, [roomId]); 

  // 소주제 리스트 API
  const loadTalkRooms = useCallback(async () => {
    try {
      const res = await talkRoomsApi(categoryCode);
      setRooms(res.data.data.content);
    } catch(err) {
      console.log(err);
    }
  }, [categoryCode]);

  // 리액션 닫기
  const handleReactions = () => {
    setAllOpenReactions(false);
  };

  // 스크롤 맨 아래로 내리기
  const scrollToBottom = () => {
    if (talkWrapRef.current && contentsAreaRef.current) {
      talkWrapRef.current.scrollTo(0, contentsAreaRef.current?.clientHeight);
    }
    setReceivedNewMessage(false);
  }

  // 판 이용동의 팝업 동의
  const handleCompleteAgree = () => {
    setIsPannAgree(false);
  }

  // 판 이용동의 팝업 체크박스 처리
  const handleChangeAgree = () => {
    setIsPannAgreeCheckbox(!isPannAgreeCheckbox)
  }

  // 판 이용약관 페이지로 이동
  const viewAgreeContract = () => {

  }
  
  useEffect(() => {
    loadTalkRoom();
    loadTalkRooms();
  }, [loadTalkRoom, loadTalkRooms]);


  useEffect(()=> {
    scrollToBottom();
    setReceivedNewMessage(true);
    if (talkWrapRef.current && contentsAreaRef.current) {
      const handleScroll = () => {
        if (talkWrapRef.current && contentsAreaRef.current && contentsAreaRef.current.clientHeight !== talkWrapRef.current?.scrollTop + talkWrapRef.current?.clientHeight) {
          setIsBtnBottom(true);
        } else {
          setIsBtnBottom(false);
        }
      }
      talkWrapRef.current.addEventListener('scroll', handleScroll);
    }
  }, []);

  return (
    <div className='wrapper talk'>
      <Header title={`10대 이야기`} isDepth={true} btns={true} />
      <RoomList rooms={rooms} categoryCode={categoryCode} />
      {
        room && categoryCode &&
        <RoomInfo room={room} />
      }
      <div className='talk_wrap' onClick={handleReactions} ref={talkWrapRef}>
        <div className='contents_area' ref={contentsAreaRef}>
          <div className='notice'>
            펑톡방이 만들어졌습니다. 
          </div>
          <div className='notice_paragragh'>
            <p>
              펑톡방 만든 시점부터 24시간 뒤에 ‘펑’ 하고 사라집니다.<br/>
              말풍선을 길게 눌러 긍정 리액션을 보여주세요!<br/>
              타인에게 불쾌감을 주거나, 부적절한 메시지는 경고 없이 삭제될 수 있으며<br/>
              운영정책에 따라 서비스 이용이 제한될 수 있습니다.
            </p>
          </div>
          <Bubble isMe={false} allOpenReactions={allOpenReactions} setAllOpenReactions={setAllOpenReactions} />
          <Bubble isMe={true} allOpenReactions={allOpenReactions} setAllOpenReactions={setAllOpenReactions} />
          <Bubble isMe={false} isReaction={true} allOpenReactions={allOpenReactions} setAllOpenReactions={setAllOpenReactions} />
          <Bubble isMe={true} isDeleted={true} allOpenReactions={allOpenReactions} setAllOpenReactions={setAllOpenReactions} />
          <Bubble isMe={false} isReported={true} allOpenReactions={allOpenReactions} setAllOpenReactions={setAllOpenReactions} />
          <Bubble isMe={false} isImage={`${process.env.PUBLIC_URL}/sample/talk_image.png`} />
        </div>
      </div>
      
      <div className='input_area'>
        {
          receivedNewMessage &&
          <div className='btn_new_wrap'>
            <YellowBorderButton type='text' text={<span className='ico arr down yellow'>새로운 메시지 보기</span>} onclick={scrollToBottom} />
          </div>

        }
        {
          // 채팅창 하단으로 가는 버튼 
          isBtnBottom &&
          <div className='btn_top_wrap'>
            <YellowBorderButton type='only_img' classNames={'arr down yellow'} onclick={scrollToBottom} text={'최하단으로 가기'} />
          </div>
        }
        <TalkInput />
      </div>

      
      {
        // 판 이용약관 동의 팝업
      isPannAgree &&
        <PannAgreeModalPortal>
          <>
            <div className='modal_header'>
              <div className='title'>
                판 이용약관 동의
              </div>
            </div>
            <div className='modal_body'>
              <p>
                펑톡방을 만들거나 대화에 참여하시려면,<br/>
                판 이용약관에 동의하셔야 합니다.<br/>
                동의 전에 이용약관을 반드시 확인 부탁드립니다.
              </p>
              <ExternalLink text={'판 이용약관 보기'} onclick={viewAgreeContract} />
              <div className='input_btn_area mb20'>
                <label>
                  <InputBtnGroup type="checkbox" name="pannAgree" value={isPannAgreeCheckbox ? '1' :'0' } checked={isPannAgreeCheckbox} onchange={handleChangeAgree} />
                  <span className='text'>동의합니다.</span>
                </label>
              </div> 
            </div>
            <div className='modal_footer'>
              <ButtonWrapper col={1}>
                <YellowButton type='text' isDisabled={!isPannAgreeCheckbox} onclick={handleCompleteAgree} text={'확인'} />
              </ButtonWrapper>
            </div>
            
          </>
        </PannAgreeModalPortal>
      }
    </div>
  );
};

export default RoomPage;
