import EmptyList from 'components/Empty/EmptyList';
import Footer from 'components/Footer/Footer';
import Header from 'components/Header/Header';
import ShapeOfRoom from 'components/Room/Shape/ShapeOfRoom';
import { WhiteButtonWithClock } from 'components/common/Button';
import Title from 'components/common/Title';
import React from 'react';
import { useNavigate } from 'react-router-dom';
const JoinedRoomPage = () => {
    const navigate = useNavigate();
    // 방 진입
    const enterTalkRoom = (id: number) => {
        navigate('/talk/room/1');
    };

    // 펑톡방 만들기
    const createNewTalkRoom = () => {
        navigate('/talk/new');
    }
    return (
        <div className='wrapper depth'>
            <Header 
                title="" 
                isDepth={true} btns={false} />
            <div className='list'>
                <div className='section'>
                    <Title text={'내가 만든 펑톡방'} isIcon={true} len={'1'} />
                    <div className='shapes'>
                        <ShapeOfRoom onclick={() => enterTalkRoom(1)} num={1} roomName={'방1'} createFlag={true} color={'lightblue'} deleteDate={'2024-06-28 00:00:00'} /> 
                    </div>
                </div>
                
                {/* 내가 만든 펑톡방이 없는 경우 */}
                <div className='section'>
                    <Title text={'내가 만든 펑톡방'} isIcon={true} len={'0'} />
                    <EmptyList title={`내가 만든 펑톡방이 없어요!`} text={'아무 이야기나 좋아요. 펑톡방을 직접 만들어 볼까요?'} isNeededBottomBtn={true} />
                    <div className='btn_area center'>
                        <WhiteButtonWithClock type="text" onclick={createNewTalkRoom} text={`펑톡방 만들기`} />
                    </div>
                </div>

                <div className='section no_bottom'>
                    <Title text={'참여 중인 펑톡방'} isIcon={true} len={'2'} />
                    <div className='shapes'>
                        <ShapeOfRoom onclick={() => enterTalkRoom(1)} num={1} roomName={'방1'} createFlag={true} color={'lightblue'} deleteDate={'2024-06-28 00:00:00'} /> 
                        <ShapeOfRoom onclick={() => enterTalkRoom(1)} num={1} roomName={'방1'} createFlag={true} color={'lightblue'} deleteDate={'2024-06-28 00:00:00'} /> 
                    </div>
                </div>

                
                {/* 참여 중인 펑톡방이 없는 경우 */}
                <div className='section no_bottom'>
                    <Title text={'참여 중인 펑톡방'} isIcon={true} len={'0'} />
                    <EmptyList title={`참여 중인 펑톡방이 없어요!`} text={'관심있는 펑톡방에 참여해서 같이 수다 떨어요.'} />
                </div>
            </div>
            <Footer />
        </div>
    );
};

export default JoinedRoomPage;