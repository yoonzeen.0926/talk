import './index.scss';
import SwiperSlideComp from 'components/SwiperSlideComp/SwiperSlideComp';
import { SwiperClass, SwiperSlide } from 'swiper/react';
import { useNavigate } from 'react-router-dom';
import Header from 'components/Header/Header';
import { useEffect, useState } from 'react';
import RoomInfoWithMsg from 'components/Room/Info/RoomInfoWithMsg';
import Footer from 'components/Footer/Footer';
import Title from 'components/common/Title';
import { MoreButton, MoreButtonOfTwoPage } from 'components/common/Button';
import EmptyList from 'components/Empty/EmptyList';
import ShapeOfRoom, { shapeProps } from 'components/Room/Shape/ShapeOfRoom';
import { randomColor } from 'util/common';
import Category, {categoryProps } from 'components/common/Category';
import { categoryApi } from 'api/section';
import useConvertVh from 'hooks/useConvertVh';

export const categoriesData = [{
  categoryCode: 'TEN',
  categoryName: '10대 이야기',
  order: 1,
  useFlag: 'Y',
}, {
  categoryCode: 'IDOL',
  categoryName: '아이돌',
  order: 2,
  useFlag: 'Y',
},{
  categoryCode: 'ENTER',
  categoryName: '엔터',
  order: 3,
  useFlag: 'Y',
},{
  categoryCode: 'CELEB',
  categoryName: '썸·연애',
  order: 4,
  useFlag: 'Y',
}, {
  categoryCode: 'LIFE',
  categoryName: '사는 얘기',
  order: 5,
  useFlag: 'Y',
}
];

const ListPage = () => {
  
  const [swiperIndex, setSwiperIndex] = useState(0);
  const [swiper, setSwiper] = useState<SwiperClass>();
  // 인기 펑톡방 카테고리
  const [categories, setCategories] = useState<categoryProps[]>(categoriesData);
  const [selectedCategory, setSelectedCategory] = useState<categoryProps>({
    categoryCode: '',
    categoryName: '',
    order:1,
    useFlag : ''
  });

  // 곧 사라지는 인기 펑톡방
  const [gonePopularList, setGonePopularList] = useState<shapeProps[]>([]);
  const [goneIdx, setGoneIdx] = useState<boolean>(true);
  
  // 랜덤 한꺼번에 돌리기
  const [shapes, setShapes] = useState<shapeProps[]>([{
    shapeNum:0, color:''
  }]);

  const navigate = useNavigate();

  useConvertVh();

  // 방 진입
  const enterTalkRoom = (id: string) => {
    navigate(`/talk/room/${id}`);
    window.scrollTo(0, 0);
  };

  // 더보기 페이지로 이동
  const viewMoreListPage = () => {
    navigate('/talk/joined');
    window.scrollTo(0, 0);
  }

  // 슬라이드
  const moveToSlide = (idx:number) => {
    setSwiperIndex(idx); 
    swiper?.slideTo(idx);
  }

  // 대주제 카테고리 API
  const loadCategories = async () => {
    try {
      const res = await categoryApi();
      setCategories(res.data.data.categories);
      setSelectedCategory(res.data.data.categories[0]);
    } catch(err) {
      console.log(err)
    }
  }

  useEffect(() => {
    let newArr = new Array(16).fill({
      shapeNum:0,
      color:''
    });
    newArr.forEach((el:any, idx:number) => {
      newArr[idx] = {
        shapeNum : Math.floor(Math.random() * 4),
        color: randomColor(idx)
      };
    });
    setShapes(newArr);
    loadCategories();
  }, []);

  

  return (
    <div className='wrapper'>
      <Header 
        title="" 
        btns={true} />
        <div className='list'>
          <div className='all popular'>
            <SwiperSlideComp onActiveIndexChange={(e:any)=>setSwiperIndex(e.realIndex)} onSwiper={(e:any) => {setSwiper(e);}}>
              <SwiperSlide>
                <RoomInfoWithMsg rank={1} roomId='3qLAe3a0SJK3P' roomName='대신 결정해드림' categoryCode='LIFE' messageCount={1324} entranceCount={27} createCmn='10060206066' deleteDate='2024.07.10 18:00:00' />
              </SwiperSlide>
              <SwiperSlide>
                <RoomInfoWithMsg rank={2} roomId='3qLAe3a0SJK3P' roomName='여기 놀러와' categoryCode='TEN' messageCount={1324} entranceCount={27} createCmn='10060206066' deleteDate='2024.07.10 20:00:00' />
              </SwiperSlide>
            </SwiperSlideComp>
            <div className='swiper_pagination'>
              <ul>
                <li className={swiperIndex === 0 ? 'active' : ''} onClick={() => moveToSlide(0)}>1</li>
                <li className={swiperIndex === 1 ? 'active' : ''} onClick={() => moveToSlide(1)}>2</li>
              </ul>
            </div>
          </div>
          {/* 참여 중인 펑톡방 */}
          <div className='section join'>
            <Title text={'참여 중인 펑톡방'} isIcon={true} />
            <div className='shapes'>
              {
                shapes[0].color !== '' &&
                <>
                  <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} num={shapes[0].shapeNum} roomName={'따라하기방'} color={shapes[0].color} deleteDate={'2024-07-28 00:30:00'} /> 
                  <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} num={shapes[1].shapeNum} roomName={'민초 존맛'} color={shapes[1].color} deleteDate={'2024-07-08 00:50:00'} /> 
                </>
              }
       
            </div>
            <div className='btn_area center'>
              <MoreButton type="text" onclick={viewMoreListPage} text={'참여 중인 펑톡방 더보기'} />
            </div>
          </div>

          {/* 인기 펑톡방 */}
          <div className='section popular'>
            <Title text={'인기 펑톡방'} isIcon={true} />
            {
              categories && <Category categories={categories || categoriesData} selectedIdx={selectedCategory.order} classNames={'nowrap'} onclick={(category:categoryProps) => setSelectedCategory(category)}  /> 
            }
            <EmptyList title={`${selectedCategory.categoryName} 인기 펑톡방이 없어요`} text={'다양한 주제로 펑톡방을 만들어 수다 떨어요.'} />
          
            <div className='shapes'>
              {
                shapes[0].color !== '' && <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} roomName={'인기많은펑톡방이야'} hideCategory={true} hideIcoMy={true} num={shapes[3].shapeNum} color={shapes[3].color} deleteDate={'2024-06-28 00:00:00'} /> 
              }

            </div>


            <div className='btn_area center'>
              <MoreButton type="text" onclick={viewMoreListPage} text={`${selectedCategory.categoryName} 인기 펑톡방 더보기`} />
            </div>
          </div>

          {/* 곧 사라지는 펑톡방 */}
          <div className='section gone'>
            <Title text={'곧 사라지는 인기 펑톡방'} isIcon={true} />
            <div className='shapes'>
              {
                shapes[0].color !== '' &&
                <>
                  <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} num={shapes[12].shapeNum} roomName={'방1'} color={shapes[12].color} deleteDate={'2024-07-10 00:00:00'} /> 
                  <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} num={shapes[13].shapeNum} roomName={'방2'}  color={shapes[13].color} deleteDate={'2024-07-10 07:00:00'} /> 
                  <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} num={shapes[14].shapeNum} roomName={'방3'}  color={shapes[14].color} deleteDate={'2024-07-10 03:00:00'} /> 
                  <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} num={shapes[15].shapeNum} roomName={'방4'}  color={shapes[15].color} deleteDate={'2024-07-10 02:00:00'} /> 
                </>
              }
            </div>
            <div className='btn_area center'>
              <MoreButtonOfTwoPage type="text" onclick={() => setGoneIdx(!goneIdx)} text={'곧 사라지는 인기 펑톡방 더보기'} currentIdx={goneIdx ? 1 : 2} />
            </div>
          </div>
          
          {/* 대주제별 최신 펑톡방 */}
          <div className='section new no_bottom'>
              <Title text={'최신 펑톡방'} isIcon={true} />
              {/* category 0 */}
              <div className={`list_of_category ${categories && categories[0].categoryCode} TEN`}>
                <Title text={(categories && categories[0].categoryName) || '10대 이야기'} isSub={true} btns={<button type="button" className='btn only_img more'>채팅방 상세로 가기</button>} />
                <div className='row'>
                  <SwiperSlideComp slidesPerView={'auto'} spaceBetween={10} grabCursor={true}>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test'} deleteDate={'2024-07-02 00:00:00'} />
                    </SwiperSlide>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test22222'} deleteDate={'2024-07-03 08:00:00'} />
                    </SwiperSlide>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test22222'} deleteDate={'2024-07-02 00:00:00'} />
                    </SwiperSlide>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test22222'} deleteDate={'2024-07-04 00:00:00'} />
                    </SwiperSlide>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test22222'} deleteDate={'2024-07-04 00:00:00'} />
                    </SwiperSlide>
                  </SwiperSlideComp>
                </div>
              </div>
              {/* category 1 */}
              <div className={`list_of_category ${categories && categories[1].categoryCode} IDOL`}>
                <Title text={(categories && categories[1].categoryName) || '아이돌'} isSub={true} btns={<button type="button" className='btn only_img more'>채팅방 상세로 가기</button>} />
                <div className='row'>
                  {/* <SwiperSlideComp slidesPerView={'auto'} spaceBetween={10} grabCursor={true}>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test'} deleteDate={'2024-06-27 18:00:00'} />
                    </SwiperSlide>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test22222'} deleteDate={'2024-06-27 21:00:00'} />
                    </SwiperSlide>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test22222'} deleteDate={'2024-06-27 21:00:00'} />
                    </SwiperSlide>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test22222'} deleteDate={'2024-06-27 21:00:00'} />
                    </SwiperSlide>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test22222'} deleteDate={'2024-06-27 21:00:00'} />
                    </SwiperSlide>
                  </SwiperSlideComp> */}
                  <EmptyList isSmall={true} text={`${categories && categories[1].categoryName} 최신 펑톡방이 없습니다.`} />
                </div>
              </div>
              {/* category 2 */}
              <div className={`list_of_category ${categories && categories[2].categoryCode} CELEB`}>
                <Title text={(categories && categories[2].categoryName) || '연예'} isSub={true} btns={<button type="button" className='btn only_img more'>채팅방 상세로 가기</button>} />
                <div className='row'>
                  <SwiperSlideComp slidesPerView={'auto'} spaceBetween={10} grabCursor={true}>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test'} deleteDate={'2024-07-04 00:00:00'} />
                    </SwiperSlide>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test22222'} deleteDate={'2024-07-02 00:00:00'} />
                    </SwiperSlide>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test22222'} deleteDate={'2024-07-02 00:00:00'} />
                    </SwiperSlide>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test22222'} deleteDate={'2024-07-02 00:00:00'} />
                    </SwiperSlide>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test22222'} deleteDate={'2024-06-30 00:00:00'} />
                    </SwiperSlide>
                  </SwiperSlideComp>
                </div>
              </div>
              {/* category 3 */}
              <div className={`list_of_category ${categories && categories[3].categoryCode} ENTER`}>
                <Title text={(categories && categories[3].categoryName) || '엔터'} isSub={true} btns={<button type="button" className='btn only_img more'>채팅방 상세로 가기</button>} />
                <div className='row'>
                  <SwiperSlideComp slidesPerView={'auto'} spaceBetween={10} grabCursor={true}>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test'} deleteDate={'2024-07-02 00:00:00'} />
                    </SwiperSlide>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test22222'} deleteDate={'2024-07-04 00:00:00'} />
                    </SwiperSlide>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test22222'} deleteDate={'2024-07-02 00:00:00'} />
                    </SwiperSlide>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test22222'} deleteDate={'2024-07-02 00:00:00'} />
                    </SwiperSlide>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test22222'} deleteDate={'2024-07-02 00:00:00'} />
                    </SwiperSlide>
                  </SwiperSlideComp>
                </div>
              </div>
              {/* category 4 */}
              <div className={`list_of_category ${categories && categories[4].categoryCode} LIFE`}>
                <Title text={(categories && categories[4].categoryName) || '사는 얘기'} isSub={true} btns={<button type="button" className='btn only_img more'>채팅방 상세로 가기</button>} />
                <div className='row'>
                  <SwiperSlideComp slidesPerView={'auto'} spaceBetween={10} grabCursor={true}>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test'} deleteDate={'2024-07-03 00:00:00'} />
                    </SwiperSlide>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test22222'} deleteDate={'2024-07-03 17:00:00'} />
                    </SwiperSlide>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test22222'} deleteDate={'2024-07-04 00:00:00'} />
                    </SwiperSlide>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test22222'} deleteDate={'2024-07-02 00:00:00'} />
                    </SwiperSlide>
                    <SwiperSlide style={{width:'150px'}}>
                      <ShapeOfRoom onclick={() => enterTalkRoom('3qLAe3a0SJK3P')} hideIcoMy={true} isSquare={true} roomName={'test22222'} deleteDate={'2024-07-02 00:00:00'} />
                    </SwiperSlide>
                  </SwiperSlideComp>
                </div>
              </div>
                
          </div>

        </div>
      <Footer />
    </div>
  );
};

export default ListPage;
