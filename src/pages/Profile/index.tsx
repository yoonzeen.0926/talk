import Header from 'components/Header/Header';
import { BasicButton } from 'components/common/Button';
import { InputTextGroup } from 'components/common/Input';
import Title from 'components/common/Title';
import React, { useEffect, useState } from 'react';
import './index.scss'
import Toast from 'components/common/Toast';

const ProfilePage = () => {
    const [readyToSubmit, setReadyToSubmit] = useState<boolean>(false);
    const reg = /^[ㄱ-ㅎㅏ-ㅣ가-힣a-zA-Z0-9!?.]+$/; // 닉네임 정규식
    const [errOfValueOfName, setErrOfValueOfName] = useState(false);
    // toast message
    const [isToast, setIsToast] = useState<boolean>(false);

    const palette_color_list = [
        {
            value:'cream',
            id:0
        }, {
            value: 'lightpink',
            id:1
        }, {
            value: 'orange',
            id:2
        }, {
            value: 'yellow', 
            id:3
        }, {
            value: 'lightblue',
            id:4
        }, {
            value:'blue',
            id:5
        }
    ]

    const palette_face_list = [
        {
            value:'face00',
            id:0
        }, {
            value:'face01',
            id:1
        }, {
            value:'face02',
            id:2
        }, {
            value: 'face03',
            id:3
        }, {
            value: 'face04', 
            id:4
        }, {
            value: 'face05', 
            id:5
        }
    ]

    const [values, setValues] = useState({
        profile: {
            color:palette_color_list[0].value,
            face: palette_face_list[0].value
        },
        nickname: '',
    })
    
    
    const handleProfile = (type:string, str:string) => {
        setValues({
            ...values,
            profile: {
                ...values.profile,
                [type]: str
            }
        })
    }

    // 닉네임 input
    const handleInput = (e:React.ChangeEvent<HTMLInputElement>) => {
        const target = e.target;
        const val = target.value;
        setValues({
            ...values, 
            nickname : val
        });
        if (val.length === 0) {
            setErrOfValueOfName(false);
            return;
        }

        // check error
        if (reg.test(val)) {
            setErrOfValueOfName(false);
        } else {
            setErrOfValueOfName(true);
        }
    }
    // input 값 지우기
    const resetValueOfName = () => {
        setValues({
            ...values,
            nickname : ''
        });
        setErrOfValueOfName(false);
    }

    // 프로필이미지 랜덤화하기
    const randomProfile = () => {
        setValues({
            ...values,
            profile: {
                color:palette_color_list[Math.floor(Math.random() * palette_color_list.length)].value,
                face:palette_face_list[Math.floor(Math.random() * palette_face_list.length)].value,
            }
        })
    }

    // 완료 버튼 클릭
    const handleSubmit = () => {
        setIsToast(true);
    }

    useEffect(() => {
        // 완료버튼 활성화
        if (values.nickname !== '' && !errOfValueOfName) {
            setReadyToSubmit(true);
        } else {
            setReadyToSubmit(false);
        }
    }, [values, errOfValueOfName]);
    return (
        <div className='wrapper depth profile'>
            <Header title='' backType='close' isDepth={true} btns={true}>
                <BasicButton type={'text'} text={'완료'} onclick={handleSubmit} isDisabled={!readyToSubmit} />
            </Header>
            <div className='section setting_profile'>
                <Title isSub={true} text={'프로필 이미지를 선택하세요.'} />
                <div className='content'>
                    <div className={`my_profile ${values.profile.color} ${values.profile.face}`}>
                        <div className='outer'>
                            <div className='inner'>
                                <div className='circle'></div>
                            </div>
                        </div>
                        <BasicButton type={'only_img'} classNames={'refresh'} onclick={randomProfile} text={'새로고침'} />
                    </div>
                    <div className=''>
                        <ul className='palette_color palette'>
                            {
                                palette_color_list.map((el:any) => (
                                    <li className={`${el.value} ${el.value === values.profile.color ? 'selected' : ''}`} key={el.id} onClick={() => handleProfile('color', el.value)}>
                                        <div className='outer'><div className='inner'><div className='circle'></div></div></div>
                                    </li>
                                ))
                            }
                        </ul>
                        <ul className='palette_face palette'>
                            {
                                palette_face_list.map((el:any) => (
                                    <li className={`${el.value} ${el.value === values.profile.face ? 'selected' : ''}`} key={el.id} onClick={() => handleProfile('face', el.value)} >
                                        <div className='outer'><div className='inner'><div className='circle'></div></div></div>
                                    </li>
                                ))
                            }
                        </ul>
                    </div>
                </div>
            </div>
            <div className='section nickname'>
                <Title isSub={true} text={'닉네임을 입력해주세요.'} />
                <div className='content'>
                    <div className='input_wrap'>
                        <InputTextGroup value={values.nickname} onchange={handleInput} max={6} isError={errOfValueOfName} 
                        reset={resetValueOfName} 
                        errorText={'조건에 맞지 않는 닉네임입니다.'}
                        help={<>한글, 영문 대소문자, 숫자만 입력 가능합니다.</>}
                        />
                    </div>
                </div>
            </div>
            {
                isToast &&
                <Toast text={'설정이 완료되었습니다.'} />
            }
        </div>
    );
};

export default ProfilePage;