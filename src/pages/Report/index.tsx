import Header from 'components/Header/Header';
import { InputBtnGroup } from 'components/common/Input';
import React, { useRef, useState } from 'react';
import './index.scss'
import { ButtonWrapper, ExternalLink, WhiteBorderButton, YellowButton } from 'components/common/Button';
import { useNavigate } from 'react-router-dom';
import useBodyScroll from 'hooks/useBodyScroll';
import PopupModalPortal, { initIsPopup } from 'components/Modal/PopupModalPortal';

type reportProps = {
    id:number;
    value:string;
    name:string;
}

const ReportPage = () => {
    const reportList:reportProps[] = [
        {
            id: 0,
            value: '0',
            name: '헌법 및 사회질서, 미풍양속에 반하는 게시물'
        },
        {
            id: 1,
            value: '1',
            name: '음란성 게시물'
        },
        {
            id: 2,
            value: '2',
            name: '개인정보 침해 및 명예훼손 게시물'
        },
        {
            id: 3,
            value: '3',
            name: '불법 광고 게시물'
        },
        {
            id: 4,
            value: '4',
            name: '도배성 게시물'
        },
        {
            id: 5,
            value: '5',
            name: '저작권 침해 게시물'
        },
        {
            id: 6,
            value: '6',
            name: '폭력/잔혹/혐오'
        },
        {
            id: 7,
            value: '7',
            name: '기타'
        },
    ]
    const [value, setValue] = useState<string>('');
    const [reason, setReason] = useState<string>('');
    const [isOnInput, setIsOnInput] = useState<boolean>(false);
    const [isPopup, setIsPopup] = useState(initIsPopup);
    const reasonRef = useRef<HTMLDivElement>(null);
    const navigate = useNavigate();
    
    // 라디오버튼 체크
    const handleCheck = (e:React.ChangeEvent<HTMLInputElement>) => {
        const target = e.target;
        const val = target.value;
        setValue(val);
        if (val !== '7') {
            const div = reasonRef?.current;
            setIsOnInput(false);
            setReason('');
            if (div) {
                div.innerHTML = '';
            }
        }
    }

    // 취소
    const handleBack = () => {
        navigate(-1);
    }

    // 기타 - 신고 사유
    const handleInput = (e:React.ChangeEvent<HTMLDivElement>) => {
        if (e.currentTarget.innerText?.length === 0) {
            setIsOnInput(false);
        } else {
            setIsOnInput(true);            
            setReason(e.target.innerText)
        }
    }

    const handleBlur = (e:any) => {
        setReason(e.currentTarget.innerText);
    }
    const handleInputWrap = () => {
        // 기타로 선택
        setValue('7');
        setIsOnInput(true);
        reasonRef.current?.focus();
    }

    const viewLinkPage = () => {
        // 링크로 이동
        // location.href = '';
    }

    // 제출
    const handleSubmit = () => {
        if (value === '') {
            setIsPopup({
                show: true,
                type: 'alert',
                text: '신고 사유를 선택해주세요.',
                action:handleClosePopup
            });
        } else if (value === '7' && reason === '') {
            setIsPopup({
                show: true,
                type: 'alert',
                text: '신고 내용을 입력해주세요.',
                action:handleClosePopup
            });
        } else {
            setIsPopup({
                show: true,
                type: 'alert',
                text: '신고가 완료되었습니다.',
                action:() => navigate(-1)
            });
        }
    }

    const handleClosePopup = () => {
        setIsPopup(initIsPopup);
    }

    useBodyScroll(false);
    return (
        <div className='wrapper depth'>
            <Header isDepth={true} title={'신고하기'} />
            <div className='select_wrap pb30'>
                <ul>
                    {
                        reportList.map((el:reportProps) => (
                            <li className={`${value === el.value ? 'selected' : ''}`} key={el.id}>
                                <div className='select_menu'>
                                    <div className='title'>
                                        <label>
                                            <span className='text_area'>
                                                {el.name}
                                            </span>
                                            <span className='input_btn_area'>
                                                <InputBtnGroup type="radio" name='report' value={el.value} checked={value === el.value} onchange={handleCheck} />
                                            </span>
                                        </label>
                                    </div>                                
                                    {
                                        el.value === '7' &&
                                        <div className='reason_area'>
                                            <div className='reason' ref={reasonRef} onInput={handleInput} onBlur={handleBlur} contentEditable suppressContentEditableWarning></div>
                                            {
                                                isOnInput ? '' : <span className='placeholder' onClick={handleInputWrap}>신고사유를 입력해주세요.</span>
                                            }
                                        </div>
                                    }

                                </div>
                            </li>
                        ))
                    }
                </ul>
            </div>
            <div className='notice report'>
                <div className='title'>
                    불법촬영물등에 대한 신고
                </div>
                <p>
                    전기통신사업법 시행령에 따른 불법촬영물등에 대한<br/>
                    신고/삭제 요청은 고객센터에서 해주세요.
                </p>
                <ExternalLink text={'불법촬영물 등 신고/삭제 요청'} onclick={viewLinkPage} />
                <p className='small'>
                    * 허위신고를 할 경우 신고자의 활동에 제한을 받을 수 있습니다.<br/>
                    이 점 유의해 주시기 바랍니다.
                </p>
            </div>
            <div className='pb80'>
                <ButtonWrapper col={2}>
                    <>
                        <WhiteBorderButton type="text" onclick={handleBack} text={'취소'} />
                        <YellowButton type="text" onclick={handleSubmit} text={'확인'} />
                    </>
                </ButtonWrapper>
            </div>
            {
                isPopup.show && 
                <PopupModalPortal size={'lg'} type='alert' textOfYes={'확인'} onclickOfYes={() => isPopup.action()}>
                    {isPopup.text}
                </PopupModalPortal>
            }
        </div>
    );
};

export default ReportPage;