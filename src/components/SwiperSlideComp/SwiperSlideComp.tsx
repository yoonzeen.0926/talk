import React from 'react';
import { Swiper } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import './style.scss'

const SwiperSlideComp = (props: any) => {
  return <Swiper {...props}>{props.children}</Swiper>;
};

export default SwiperSlideComp;
