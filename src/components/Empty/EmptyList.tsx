import React from 'react';
import './EmptyList.scss'

type emptyListProps = {
    title?: string;
    text: string;
    isSmall?: boolean;
    isNeededBottomBtn?: boolean;
}

const EmptyList = (props:emptyListProps) => {
    return (
        <div className={`empty_list ${props.isSmall ? 'small' : ''} ${props.isNeededBottomBtn? 'is_bottom_btn' : ''}`}>
            <div className='notice'>
                {
                    props.title && <h3>{props.title}</h3>
                }
                <div className='text'>
                    {props.text}
                </div>
            </div>
        </div>
    );
};

export default EmptyList;