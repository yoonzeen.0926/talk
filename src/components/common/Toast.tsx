import React from 'react';

type toastProps = {
    text:string;
}

const Toast = (props:toastProps) => {
    return (
        <div className='toast'>
            <div className='inner'>
                {props.text}
            </div>
        </div>
    );
};

export default Toast;