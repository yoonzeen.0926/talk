import React, { ReactElement } from 'react';

type titleProps = {
    text:string;
    isSub?: boolean;
    len?: string;
    isIcon?: boolean;
    btns?:ReactElement;
}

const Title = (props:titleProps) => {
    return (
        <div className={`title ${props.isSub ? 'sub' : ''}`}>
            <div className={`text ${props.isIcon ? 'is_icon' : ''}`}>
                {props.text} {props?.len ? <span className={props?.len === '0' ? 'color_gray' : 'color_yellow'}>{props?.len}</span> : ''}
            </div>
            <div className='btns'>
                {
                    props.btns
                }
            </div>
        </div>
    );
};

export default Title;