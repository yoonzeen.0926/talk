import React, { useState } from 'react';

type tabProps = {
    krName: string;
    enName: string;
    id:number;
}

type tabsProps = {
    tabs:tabProps[];
    position:string;
    classNames?: string;
}

const BoxTab = (props:tabsProps) => {
    const [active, setActive] = useState(0);
    const handleSorting = (idx:number) => {
        setActive(idx);
    }
    return (
        <div className={`tabs ${props.classNames ? props.classNames : ''} ${props.position}`}>
            <ul>
                {
                    props.tabs.map((el:tabProps, idx:number) => <li key={el.id} onClick={() => handleSorting(el.id)} className={`tab ${active === el.id ? 'on' : ''}`}>{el.krName}</li>)
                }
            </ul>
        </div>
    );
};

export default BoxTab;