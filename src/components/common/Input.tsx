import React, { ReactElement, useState } from 'react';
import { BasicButton } from './Button';

export type inputBtnProps = {
    name:string;
    value:string | number;
    onchange:Function;
    checked: boolean;
    type:string;

    classNames?:string;
}
export type inputTextProps = {
    value:string | number;
    onchange:Function;
    classNames?: string;
    max?: number;
}

export type inputFileProps = {
    name:string;
    value: string;
    onchange:Function;

    classNames?:string;
}

export type inputTextGroupProps = {
    value:string | number;
    onchange:Function;
    classNames?: string;
    max?: number;
    isError?: boolean;
    errorText?:string | ReactElement;
    help?:string | ReactElement;
    reset?:Function;
}

// type="radio", type="checkbox"
export const InputBtnGroup = (props:inputBtnProps) => {
    return (
        <>
            <input type={props.type} name={props.name} value={props.value} onChange={(e) => props.onchange(e)} />
            <span className={`btn ${props.type} ${props.classNames} ${props.checked ? 'on' : ''}`}></span>
        </>
    );
};

// type="text"
export const InputText = (props:inputTextProps) => {
    const [isActive, setIsActive] = useState(false);
    const handleClick = () => {
        setIsActive(true);
    }
    const handleBlur = () => {
        setIsActive(false);
    }
    return (
        <div className={`input_text_area ${isActive ? 'active' : ''} ${props.classNames ? props.classNames : ''}`}>
            <input type='text' value={props.value} onChange={(e) => props.onchange(e)} onClick={handleClick} onBlur={handleBlur} maxLength={props.max ? props.max : 9999} />
        </div>
    );
};

// 에러메시지 띄울 수 있는 input group
export const InputTextGroup = (props:inputTextGroupProps) => {
    return (
        <div className='input_text_group'>
            <div className='input_wrap'>
                <InputText value={props.value} onchange={(e:React.ChangeEvent<HTMLInputElement>) => props.onchange(e)} max={props.max} classNames={props.isError ? 'err' : ''} />
                {
                    props.value.toString().length > 0 ? <BasicButton type="only_img" text='지우기' classNames='del' onclick={() => props.reset && props.reset()} /> : ''
                }
            </div>
            <div className='len_wrap'>
                {
                    props.isError && props.value.toString().length > 0 && 
                    <div className='err'>{props.errorText}</div>
                }
                <div className='len'>
                    <span className='color_white'>{props.value.toString().length}</span> / {props.max}
                </div>
            </div>
            <div className='help_text'>
                {props.help}
            </div>
        </div>
    )
}

// 파일 첨부 input button
export const InputFileGroup = (props:inputFileProps) => {
    return (
        <label>
            <input type="file" name={props.name} value={props.value} onChange={(e) => props.onchange(e)} />
            <span className={`btn only_img photo ${props.classNames ? props.classNames : ''}`}></span>
        </label>
    )
}