import React from 'react';

export type categoryProps = {
    categoryCode:string;
    categoryName: string;
    order:number;
    useFlag: string;
}

type categoriesProps = {
    selectedIdx?: number | undefined;
    categories: categoryProps[];
    onclick: Function;
    classNames?: string;
}

const Category = (props:categoriesProps) => {
    return (
        <div className='category_wrap'>
            <div className={`categories ${props.classNames ? props.classNames : ''}`}>
                {
                    props.categories.map((category:categoryProps, idx:number) => (
                        <div key={category.order} className={`category ${category.order === props.selectedIdx ? 'selected' : ''}`} onClick={() => props.onclick(category)}>{category.categoryName}</div>
                    ))
                }
            </div>
        </div>
    );
};

export default Category;