import React, { ReactElement } from 'react';
type buttonProps = {
    text: string | ReactElement;
    onclick: Function;
    type: string;  // text / only_img

    currentIdx?:number;
    classNames? : string | '';
    isDisabled?:boolean;
}

// 버튼 감싸는 element type
type buttonWrapperProps = {
    children:ReactElement;
    col?:number;
}

type linkProps = {
    onclick:Function;
    text:string;
}

// 일반 버튼(text, img)
export const BasicButton = (props:buttonProps) => {
    return(
        <button type="button" className={`btn ${props.type} origin ${props.classNames? props.classNames : ''} ${props.isDisabled ? 'disabled' : ''}`} onClick={() => props.onclick()} disabled={props.isDisabled}>{props.text}</button>
    )
}

// 더보기 버튼
export const MoreButton = (props:buttonProps) => {
    return (
        <>
          <button type="button" className={`btn ${props.type} more`} onClick={() => props.onclick()}>{props.text}</button>  
        </>
    );
};

// 1/2 <-> 2/2 버튼
export const MoreButtonOfTwoPage = (props:buttonProps) => {
    return (
        <>
          <button type="button" className={`btn ${props.type} more`} onClick={() => props.onclick()}><span className='ico refresh'>{props.text}</span><span className='page'><span className='current'>{props.currentIdx}</span>/2</span></button>  
        </>
    );
};

// 화이트 배경의 시계 달린 버튼
export const WhiteButtonWithClock = (props:buttonProps) => {
    return (
        <>
            <button type="button" className={`btn ${props.type} white`} onClick={() => props.onclick()}><span className='ico clock black'>{props.text}</span></button>  
        </>
    )
}

// 노란 테두리의 버튼
export const YellowBorderButton = (props:buttonProps) => {
    return (
        <>
            <button type="button" className={`btn ${props.type} border_yellow ${props.classNames}`} onClick={() => props.onclick()}>{props.text}</button>
        </>
    )
}

// 화이트 보더의 버튼
export const WhiteBorderButton = (props:buttonProps) => {
    return (
        <>
            <button type="button" className={`btn ${props.type} border_white`} onClick={() => props.onclick()}><span>{props.text}</span></button>  
        </>
    )
}

// 노란 배경의 버튼
export const YellowButton = (props:buttonProps) => {
    return (
        <>
            <button type="button" className={`btn ${props.type} yellow`} onClick={() => props.onclick()} disabled={props.isDisabled}><span>{props.text}</span></button>  
        </>
    )
}

// 팝업 내 버튼
export const PopupButton = (props:buttonProps) => {
    return (
        <>
            <button type="button" className={`btn ${props.type} small ${props.classNames ? props.classNames : ''}`} onClick={() => props.onclick()}><span>{props.text}</span></button>
        </>
    )
}

// 버튼 wrapper
export const ButtonWrapper = (props:buttonWrapperProps) => {
    return (
        <div className={`btn_wrapper col_${props.col}`}>
            {props.children}
        </div>
    )
}

// 외부 링크로 이동
export const ExternalLink = (props:linkProps) => {
    return (
        <div className='link external'>
            <div className='ico blue_pop'><div className='underlined color_lightblue' onClick={() => props.onclick()}>{props.text}</div></div>
        </div>
    )
}