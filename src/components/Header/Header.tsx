import React, { ReactElement, useState } from 'react';
import './Header.scss';
import { useNavigate } from 'react-router-dom';

type headerProps = {
    title: string;
    isDepth?: boolean;
    btns?: ReactElement | boolean;
    backType?: string; // back / close
    backEvent?: Function;
    children?: ReactElement
}

const Header = (props:headerProps) => {
    const navigate = useNavigate();
    const [isMiniModal, setIsMiniModal] = useState<boolean>(false);

    // 헤더 내 오른쪽 버튼 모음
    const isBtnsInHeader = () => {
        // 새 펑톡방 만들기 페이지로 이동
        const createNewRoomPage = () => {
        navigate('/talk/new');
        }

        // 미니모달 켜기/끄기
        const controlSelectMenu = () => {
        setIsMiniModal(!isMiniModal);
        }
  
        // 프로필 설정 페이지로 이동
        const viewProfilePage = () => {
          navigate('/profile');
        }
        return (
        <>
            <button type="button" className='btn only_img in_circle create' onClick={createNewRoomPage}>새 펑톡방 만들기</button>
            <button type="button" className='btn only_img in_circle option' onClick={controlSelectMenu}>옵션</button>
            {
            isMiniModal &&
            <div className='modal mini right'>
                <ul>
                {/* 로그인일 때 보여주기 */}
                <li onClick={viewProfilePage}>프로필 설정</li>
                {/* // 로그인일 때 보여주기 */}
                <li>공지사항</li>
                <li>운영정책</li>
                </ul>
            </div>
            }
        </>
        )
    }
    return (
        <div className='header'>
            <div className='header_inner'>
                {
                    props.isDepth ?
                    <>
                        <button type="button" className={`btn only_img ${props.backType === 'close' ? 'close' : 'back'}`} aria-label='뒤로가기' onClick={() => props.backEvent ? props.backEvent : navigate(-1)}>뒤로가기</button>
                        <div className='page_name'>{props.title}</div>
                    </>
                        :
                    <div className='logo_area'>
                        <h1><span className='logo'>Nate</span><span className='text main'>펑톡</span></h1>
                        <a href="//pann.nate.com" title="네이트판" className='text sub'>판</a>
                    </div>
                }
                
                {
                    props.btns &&
                    <div className='btns'>
                        { props.children ? props.children : isBtnsInHeader() }
                    </div>
                }
            </div>
        </div>
    );
};

export default Header;
