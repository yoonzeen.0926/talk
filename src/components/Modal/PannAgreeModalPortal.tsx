import ReactDOM from 'react-dom';
import './ModalPortal.scss'
import { ReactElement } from 'react';

type modalProps = {
    isOn? : boolean;
    children : ReactElement | string;
}

const PannAgreeModalPortal = ({ children }:modalProps) => {
    const el = document.getElementById('pannAgreeModal') as HTMLDivElement;
    return ReactDOM.createPortal(<div className={`modal`}><div className='bg'></div><div className='contents'>{children}</div></div>, el);
};

export default PannAgreeModalPortal; 