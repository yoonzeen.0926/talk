import ReactDOM from 'react-dom';
import './ModalPortal.scss'
import { ReactElement } from 'react';
import useBodyScroll from 'hooks/useBodyScroll';
import { ButtonWrapper, PopupButton } from 'components/common/Button';

type popupModalProps = {
    type : string;  // alert / confirm
    size: string; // lg, md, sm
    textOfYes:string | ReactElement;
    textOfNo?:string | ReactElement;
    onclickOfYes: Function;
    onclickOfNo?: Function;
    children : ReactElement | string;
}
export const initIsPopup = {
    show: false,
    type: 'alert',
    text: '',
    action: () => {}
};
const PopupModalPortal = ({ type, size, onclickOfYes, onclickOfNo, textOfYes, textOfNo = '', children }:popupModalProps) => {
    const el = document.getElementById('popupModal') as HTMLDivElement;
    useBodyScroll(true);
    return ReactDOM.createPortal(
        <div className={`popup ${type} ${size}`}>
            <div className='bg'></div>
            <div className='contents'>
                <div className='text_wrap'>
                    {children}
                </div>
                {
                    type === 'alert' ? 
                    <PopupButton type="text" onclick={() => onclickOfYes()} text={textOfYes} />
                    :
                    <ButtonWrapper col={2}>
                        <>
                            <PopupButton type="text" onclick={() => onclickOfNo && onclickOfNo()} text={textOfNo || ''} classNames={'no_border'} />
                            <PopupButton type="text" onclick={() => onclickOfYes()} text={textOfYes || ''} />
                        </>
                    </ButtonWrapper>
                }
            </div>
        </div>
        , el);
};

export default PopupModalPortal; 