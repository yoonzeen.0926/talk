import ReactDOM from 'react-dom';
import './Portal.scss'

const PungPortal = ({ children, isOn }:any) => {
    const el = document.getElementById('chatEndModal') as HTMLDivElement;
    return ReactDOM.createPortal(<div className={`${isOn ? 'on' : ''} is_pung`}>펑!!</div>, el);
};

export default PungPortal;