import React from 'react';
import './Footer.scss'

const Footer = () => {
    return (
    <div id="footer" role="contentinfo">			
		<div className="footer">			
			<div className="svcBtn">
				<a href="#none">네이트 홈</a><a href="#none">로그아웃</a><a href="#none">PC버전</a><a href="#none">전체서비스</a>			
			</div>
			<div className="svcInfo">
	        	<a href="https://www.nate.com/policy/legal.html" >서비스 이용약관</a>
	        </div>
	        <div className="copyright">
	        	<span>© SK Communications</span><a className="cscenter" href="https://m.helpdesk.nate.com/">고객센터</a>
	        </div>
		</div>
	</div>
    );
};

export default Footer;