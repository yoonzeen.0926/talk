import React, { useCallback, useRef, useState } from 'react';
import { convertTimeInRoom } from 'util/time';
import './Bubble.scss'
import { LongPressEventType, useLongPress } from 'use-long-press';
import { convertUnit } from 'util/unit';
import { BasicButton } from 'components/common/Button';
import PopupModalPortal, { initIsPopup } from 'components/Modal/PopupModalPortal';
import { useNavigate } from 'react-router-dom';

export type bubbleProps = {
    isMe: boolean;

    isImage?: string;
    isDeleted?: boolean;
    isReported?: boolean;
    isReaction?: boolean;
    isOpenReaction?:boolean;

    allOpenReactions?: boolean;
    setAllOpenReactions?: Function;
}

const Bubble = (props:bubbleProps) => {
    const [openReactions, setOpenReactions] = useState<boolean>(false);
    const bubbleRef = useRef<HTMLDivElement>(null);
    const [openModal, setOpenModal] = useState<boolean>(false);
    // popup
    const [isPopup, setIsPopup] = useState(initIsPopup);
    const navigate = useNavigate();

    // 리액션 - long press
    const callback = useCallback(() => {
        setOpenReactions(true);
        if (props.setAllOpenReactions) {
            props.setAllOpenReactions(true);
        }
    }, [props]);
    const handlers = useLongPress(callback, {
        threshold: 500,
        cancelOnMovement: false,
        cancelOutsideElement: true,
        detect: LongPressEventType.Pointer
    });

    // 이미지 상세보기 열기
    const openDetailImageModal = () => {
        setOpenModal(true);
    }

    // 이미지 상세보기 닫기
    const closeDetailImageModal = () => {
        setOpenModal(false);
    }

    // 메시지 신고
    const handleReport = () => {
        navigate('/report');
    }

    // 메시지 삭제
    const handleDelete = () => {
        setIsPopup({
            show: true,
            type:'confirm',
            text: '메시지를 삭제하시겠습니까?',
            action:() => {
                handleClosePopup();
            }
        });
    }
    
    // 팝업 닫기
    const handleClosePopup = () => {
        setIsPopup(initIsPopup);
    }
    return (
        <div className='bubble'>
            <div className={`bubble_inner ${props.isMe ? 'me' : 'others'} ${props.isDeleted ? 'deleted' : ''} ${props.isReported ? 'reported' : ''}`}>
                {
                    !props.isMe && 
                    <div className='profile'>
                        <img src={`${process.env.PUBLIC_URL}/sample/ico_talker_sample.svg`} alt='test' />
                    </div>
                }
                <div className='talker'>
                    <div className='info'>
                        {
                            !props.isMe && 
                            <span className='name'>계획적인 기린</span>
                        }
                        <span className='date'> {convertTimeInRoom('2024-05-18 14:30:00')}</span>
                        {
                            props.isMe ? 
                            props.isDeleted ? '' : <BasicButton type='text' classNames={'inline_text delete'} text={'삭제'} onclick={handleDelete} /> :
                            props.isReported ? '' : <BasicButton type='text' classNames={'inline_text report'} text={'신고'} onclick={handleReport} />
                        }
                    </div>
                    <div className={`contents ${props.isImage ? 'image' : ''}`}>
                        <div className='contents_inner' ref={bubbleRef} {...handlers()}>
                            {
                                (props.isDeleted && '삭제된 메시지입니다.') ||
                                (props.isReported && '신고한 메시지입니다.') ||
                                (props.isImage && <img src={props.isImage} alt={'이미지'} onClick={openDetailImageModal} />) ||
                                '뉴진스 애정은 찐인거 같음...'
                            }
                        </div>
                    </div>
                    {/* Select Reactions */}
                    {
                        openReactions && props.allOpenReactions &&
                        <div className='reactions send'>
                            <ul>
                                <li><button type='button' className='reaction reaction01'>리액션-하트</button></li>
                                <li><button type='button' className='reaction reaction02'>리액션-체크</button></li>
                                <li><button type='button' className='reaction reaction03'>리액션-웃음</button></li>
                                <li className='selected'><button type='button' className='reaction reaction03'>리액션-웃음</button></li>
                                <li><button type='button' className='reaction reaction03'>리액션-웃음</button></li>
                            </ul>
                        </div>
                    }
                    {/* Reactions */}
                    {
                        props.isReaction &&
                        <div className='reactions receive'>
                            <ul>
                                <li className='selected'><div className='reaction reaction01'>리액션-하트</div><div className='count'>{convertUnit(990000)}</div></li>
                                <li><div className='reaction reaction02'>리액션-체크</div><div className='count'>{convertUnit(999)}</div></li>
                            </ul>
                        </div>
                    }
                </div>
            </div>
            {
                openModal && props.isImage && 
                <div className='modal detail_image'>
                    <div className='modal_header'>
                        <div className='title'>
                            <div className='nickname'>계획적인 기린</div>
                            <div className='time'>오후 2:43</div>
                        </div>
                        <BasicButton type={'only_img'} classNames='close' onclick={closeDetailImageModal} text={'닫기'} />
                    </div>
                    <div className='modal_body'>
                        <img src={props.isImage} alt='이미지 보기' />
                    </div>
                </div>
            }
            {
                isPopup.show && 
                <PopupModalPortal size={'sm'} type={isPopup.type} textOfYes={'확인'} textOfNo={'취소'} onclickOfYes={() => isPopup.action()} onclickOfNo={handleClosePopup}>
                    {isPopup.text}
                </PopupModalPortal>
            }
        </div>
    );
};

export default Bubble;