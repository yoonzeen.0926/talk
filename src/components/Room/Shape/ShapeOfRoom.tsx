import React from 'react';
import './ShapeOfRoom.scss'
import ProgressBar from 'components/Room/ProgressBar/ProgressBar';
import { convertUnit } from 'util/unit';

export type shapeProps = {
    shapeNum:number;
    color:string;
}

export type shapeOfRoomProps = {
    onclick?: Function;
    hideCategory?:boolean;
    hideIcoMy?: boolean;
    isSquare?:boolean;
    isWide?:boolean;
    num?:number;
    color?:string;

    // room으로 통일시킬 것 
    roomName:string;
    deleteDate:string;
    messageCount?:number;
    createFlag?: boolean;
}

const ShapeOfRoom = (props:shapeOfRoomProps) => {
    const handleClick = () => {
        if (props.onclick) {
            props.onclick();
        }
    }
    return (
        <div className={`shape_wrap ${props.hideCategory ? 'hide_category' : ''} ${props.isSquare ? 'square' : ''} ${props.isWide ? 'wide' : ''}`}>
            {
                !props.isSquare &&
                <div className={`shape shape_${props.num} ${`bg_${props.color}`}`}></div>
            }
            {
                new Date(props.deleteDate) <= new Date() ? <div className='popped'></div> : '' 
            }
            <div className='text_wrap' onClick={handleClick}>
                {
                    (!props.hideCategory && !props.isSquare) &&
                    <div className={`${props.hideIcoMy ? 'hide_my' : ''} category_wrap ${props.createFlag ? 'my_room' : ''}`}>{ !props.hideIcoMy && <span className='ico my'>내가 만든 방</span>}<span className='category'>10대 이야기</span></div>
                }
                <div className='text'>{props.roomName}</div>
                <div className='details'>
                    <ProgressBar deleteDate={props.deleteDate} />
                    {props.messageCount ? <div className='message_count'>메시지 {convertUnit(props.messageCount)} 개</div> : ''}
                </div>
            </div>
            
        </div>
    );
};

export default ShapeOfRoom;