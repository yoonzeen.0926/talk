import React, { useRef, useState } from 'react';
import './TalkInput.scss'

// sample
import { BasicButton } from 'components/common/Button';
import { InputFileGroup } from 'components/common/Input';
import PopupModalPortal, { initIsPopup } from 'components/Modal/PopupModalPortal';
import Toast from 'components/common/Toast';


export type talkInputProps = {
    inputValue?: string;
}

const TalkInput = (props:talkInputProps) => {
    const [talkValue, setTalkValue] = useState<string>(props.inputValue ? props.inputValue : '');
    // 이모지 창 열기
    const [openEmoji, setOpenEmoji] = useState<boolean>(false); 
    // 선택된 이모지
    const [selectedEmoji, setSelectedEmoji] = useState({
        imgSrc: '',
        value:'',
        show:false
    });
    // talk_input_inner border-radius 조절
    const [isInputLines, setIsInputLines] = useState<boolean>(false);
    // placeholder
    const [isOnInput, setIsOnInput] = useState<boolean>(false);
    // 보낼 이미지
    const [selectedFile, setSelectedFile] = useState(false);
    // 완료 버튼
    const [availableToSend, setAvailableToSend] = useState<boolean>(false);
    const inputRef = useRef<HTMLDivElement>(null);
    // alert popup
    const [isPopup, setIsPopup] = useState(initIsPopup);
    // toast message
    const [isToast, setIsToast] = useState<boolean>(false);

    // 채팅창 입력 - 텍스트필드
    const handleInput = (e:React.ChangeEvent<HTMLDivElement>) => {
        if (inputRef.current) {
            const style = getComputedStyle(inputRef.current);
            const h = style.height;
            const numOfh = parseInt(h.split('px')[0]);
            setIsInputLines(numOfh > 30);
        }

        if (e.currentTarget.innerText?.length === 0) {
            setIsOnInput(false);
            setAvailableToSend(false);
        } else if (e.currentTarget.innerText?.length > 100) {
            onToast();   // 입력 가능한 글자 수 초과 토스트메시지
            if (inputRef.current) {
                inputRef.current.innerText = talkValue;
                blurInput();
            }
        } else {
            setIsOnInput(true);            
            setTalkValue(e.target.innerText)
            setAvailableToSend(true);
        }
        console.log(e.currentTarget.innerText.length)
    }

    // 채팅창 입력 - 텍스트필드 - 블러
    const handleBlur = (e:any) => {
        setTalkValue(e.currentTarget.innerText);
    }

    // 채팅창 입력 - 텍스트필드 포커스
    const handleInputWrap = () => {
        setIsOnInput(true);
        inputRef.current?.focus();
    }

    // 이모지 선택 창 open
    const toggleEmoji = () => {
        setOpenEmoji(!openEmoji);
        // 이모지 선택 창 닫을 때 보내기 버튼 비활성화
        if (openEmoji) {
            setAvailableToSend(false);
        }
    }

    // 팝업 닫기
    const handleClosePopup = () => {
        setIsPopup(initIsPopup);
    }

    // 채팅창 입력 - 텍스트필드 포커스 해제
    const blurInput = () => {
        inputRef.current?.blur();
    }

    // 텍스트필드 초기화
    const resetBubble = () => {
        const cur = inputRef.current;
        setTalkValue('');
        setIsOnInput(false);
        setAvailableToSend(false);
        setSelectedFile(false);
        if (cur) {
            cur.innerHTML = '';
        }
    }

    // 토스트 
    const onToast = () => {
        setIsToast(true);

        setTimeout(() => {
            setIsToast(false);
        }, 2000);
    }

    const handleAddPhoto = (e:React.ChangeEvent<HTMLInputElement>) => {
        const target = e.target;
        const files = target.files ? target.files : null;
        if (files && inputRef.current) {
            const file = files[0];
            if (file.type.split('/').at(0) !== 'image') {
                setIsPopup({
                    show: true,
                    type:'alert',
                    text: '지원하지 않는 확장자 입니다.',
                    action:handleClosePopup
                });
                blurInput();
            } else {
                setSelectedFile(true);
                setIsOnInput(true);
                setAvailableToSend(true);
            }
        }
    }

    // 이모지 선택 중
    const viewSelectingEmoji = (imgSrc:string, value:string) => {
        setSelectedEmoji({
            imgSrc,
            value,
            show:true
        });
        setAvailableToSend(true);
    }

    // 선택된 이모지 없애기
    const resetSelectedEmoji = () => {
        setSelectedEmoji({
            imgSrc: '',
            value: '',
            show:false
        })
        setAvailableToSend(false);
    }

    return (
        <div className={`talkinput_wrap ${openEmoji ? 'open_emoji' : ''}  ${selectedEmoji.show ? 'open_emoji_selected' : ''}`}>
            {
                isToast && 
                <Toast text={'입력 가능한 글자수를 초과하였습니다.'} />
            }
            <div className='input_emoji_area'>
                {
                    openEmoji &&
                    <div className={`emoji_select_wrap`}>
                        
                        {
                            selectedEmoji.show ? 
                            <div className='emoji_selected_wrap'>
                                <BasicButton type={'only_img'} classNames='close' onclick={resetSelectedEmoji} text={'닫기'} />
                                <div className='emoji'>
                                    <img src={selectedEmoji.imgSrc} alt={'선택된 이모지'} />
                                </div>
                            </div>
                            :''
                        }
                        <div className='tab'>
                            <ul>
                                <li className='selected'>
                                    <div className='sample1 sample'></div>
                                </li>
                                <li>
                                    <div className='sample2 sample'></div>
                                </li>
                                <li>
                                    <div className='sample3 sample'></div>
                                </li>
                            </ul>
                        </div>
                        <div className='emojies'>
                            <ul>
                                <li className={selectedEmoji.show ? selectedEmoji.value === 'emoji0'? 'selected' : 'not_selected' : ''} onClick={() => viewSelectingEmoji(`${process.env.PUBLIC_URL}/sample/emoji_sample00.png`, 'emoji0')}><div className='sample0 sample'><img src={`${process.env.PUBLIC_URL}/sample/emoji_sample00.png`} alt='emoji sample' /></div></li>
                                <li className={selectedEmoji.show ? selectedEmoji.value === 'emoji1'? 'selected' : 'not_selected' : ''} onClick={() => viewSelectingEmoji(`${process.env.PUBLIC_URL}/sample/emoji_sample01.png`, 'emoji1')}><div className='sample1 sample'><img src={`${process.env.PUBLIC_URL}/sample/emoji_sample01.png`} alt='emoji sample' /></div></li>
                                <li className={selectedEmoji.show ? selectedEmoji.value === 'emoji2'? 'selected' : 'not_selected' : ''} onClick={() => viewSelectingEmoji(`${process.env.PUBLIC_URL}/sample/emoji_sample02.png`, 'emoji2')}><div className='sample2 sample'><img src={`${process.env.PUBLIC_URL}/sample/emoji_sample02.png`} alt='emoji sample' /></div></li>
                                <li className={selectedEmoji.show ? selectedEmoji.value === 'emoji3'? 'selected' : 'not_selected' : ''} onClick={() => viewSelectingEmoji(`${process.env.PUBLIC_URL}/sample/emoji_sample03.png`, 'emoji3')}><div className='sample3 sample'><img src={`${process.env.PUBLIC_URL}/sample/emoji_sample03.png`} alt='emoji sample' /></div></li>
                                <li className={selectedEmoji.show ? selectedEmoji.value === 'emoji4'? 'selected' : 'not_selected' : ''} onClick={() => viewSelectingEmoji(`${process.env.PUBLIC_URL}/sample/emoji_sample04.png`, 'emoji4')}><div className='sample4 sample'><img src={`${process.env.PUBLIC_URL}/sample/emoji_sample04.png`} alt='emoji sample' /></div></li>
                                <li className={selectedEmoji.show ? selectedEmoji.value === 'emoji5'? 'selected' : 'not_selected' : ''} onClick={() => viewSelectingEmoji(`${process.env.PUBLIC_URL}/sample/emoji_sample05.png`, 'emoji5')}><div className='sample5 sample'><img src={`${process.env.PUBLIC_URL}/sample/emoji_sample05.png`} alt='emoji sample' /></div></li>
                                <li className={selectedEmoji.show ? selectedEmoji.value === 'emoji6'? 'selected' : 'not_selected' : ''} onClick={() => viewSelectingEmoji(`${process.env.PUBLIC_URL}/sample/emoji_sample06.png`, 'emoji6')}><div className='sample6 sample'><img src={`${process.env.PUBLIC_URL}/sample/emoji_sample06.png`} alt='emoji sample' /></div></li>
                                <li className={selectedEmoji.show ? selectedEmoji.value === 'emoji7'? 'selected' : 'not_selected' : ''} onClick={() => viewSelectingEmoji(`${process.env.PUBLIC_URL}/sample/emoji_sample07.png`, 'emoji7')}><div className='sample7 sample'><img src={`${process.env.PUBLIC_URL}/sample/emoji_sample07.png`} alt='emoji sample' /></div></li>
                            </ul>
                        </div>
                    </div>
                }
                <div className='talk_input_wrap'>
                    <div className='input_btn_area btn_photo_wrap'>
                        <InputFileGroup name={'file'} value={''} onchange={handleAddPhoto} />
                    </div>
                    <div className='talk_input'>
                        <div className={`talk_input_inner ${selectedFile ? 'image' : ''} ${isInputLines ? 'lines' : ''}`}>
                            <div className='input_wrap'>
                                {
                                    // image 
                                    selectedFile ? <span className='selected_img'><img src={`${process.env.PUBLIC_URL}/sample/talk_image.png`} alt={'이미지'}/><BasicButton type='only_img' classNames='img_del' onclick={resetBubble} text={'삭제'} /></span> : 

                                    <>
                                        <div className='input' ref={inputRef} onInput={handleInput} onBlur={handleBlur} contentEditable suppressContentEditableWarning></div>
                                        {
                                            // placeholder
                                            isOnInput ? '' : <span className='placeholder' onClick={handleInputWrap}>펑톡 대화에 참여해보세요!</span>
                                        }
                                    </>
                                }
                            </div>
                            <div className='btn_area btn_emoji_wrap'>
                                <button type="button" className={`btn only_img emoji ${openEmoji ? 'selected' : ''}`} onClick={toggleEmoji}>이모지 선택하기</button>
                            </div>
                            <div className='btn_area'>
                                <button type="button" className={`btn only_img send ${availableToSend ? 'on' : ''}`}>전송하기</button>
                            </div>
                        </div>
                    </div>
                </div>
                {
                    isPopup.show && 
                    <PopupModalPortal size={'lg'} type='alert' textOfYes={'확인'} onclickOfYes={() => isPopup.action()}>
                        {isPopup.text}
                    </PopupModalPortal>
                }
            </div>
        
        </div>
    );
};

export default TalkInput;