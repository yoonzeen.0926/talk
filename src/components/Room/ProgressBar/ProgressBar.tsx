import React, { useState } from 'react';
import './ProgressBar.scss'
import { calculateCountDown, convertPercent } from 'util/time';

export type progressProps = {
    deleteDate: string;
    type?:string;
}

const ProgressBar = (props:progressProps) => {
    const [remainTime, setRemainTime] = useState<string>();

    // 1초마다 갱신
    let timer:any;
    let toggle = true; 

    if (toggle && remainTime !== '00:00:00') {
        timer = setInterval(() => {
            setRemainTime(calculateCountDown(props.deleteDate));
        }, 1000);
        toggle = false;
    } else {
        clearInterval(timer);
        toggle = true;
    }
    return (
        <>
            <div className={`progress_bar ${props.type ? props.type : ''}`}><span className='indicator' style={{'width' : convertPercent(props.deleteDate)}}></span><span className='read_only'>남은 시간</span><span className='ico clock small time'>{remainTime}</span></div>
        </>
    );
};

export default ProgressBar;