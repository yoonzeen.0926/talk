import React, { useState } from 'react';
import { calculateCountDown } from 'util/time';
import './RoomInfoWithMsg.scss'
import { convertUnit } from 'util/unit';
import { roomProps } from '../List/RoomList';


const RoomInfoWithMsg = (props:roomProps) => {
    const [remainTime, setRemainTime] = useState<string>();

    const convertCategory = () => {
        let cate = '';
        if (props.categoryCode === 'TEN') {
            cate = '10대 이야기';
        } else if (props.categoryCode === 'IDOL') {
            cate = '아이돌';
        } else if (props.categoryCode === 'CELEB') {
            cate = '연예';
        } else if (props.categoryCode === 'ENTER') {
            cate = '엔터';
        } else if (props.categoryCode === 'LIFE') {
            cate = '사는 얘기';
        } else { cate = ''; }
        return cate;
    }

    // 1초마다 갱신
    let timer:any;
    let toggle = true; 

    if (toggle && remainTime !== '00:00:00') {
        timer = setInterval(() => {
            setRemainTime(calculateCountDown(props.deleteDate));
        }, 1000);
        toggle = false;
    } else {
        clearInterval(timer);
        toggle = true;
    }

    return (
        <div className='room_info'>
            <div className='talk_room'>
                <div className='clock_area'>
                    <span className='ico clock'><span className='time'>{remainTime}</span></span>
                </div>
                <div className='info'>
                    <div className='name'>{props.roomName}</div>
                    <div>
                        <span className='category'>{convertCategory()}</span> 
                        <span className='count'>메시지 {convertUnit(props.messageCount)}</span>
                    </div>
                </div>
            </div>
            <div className='talk_summary'>
                <div className='item'>
                    <div className='username'>푸바오</div>
                    <div className='contents'>오늘 기자회견 레알 속시원 했음오늘 기자회견 레알 속시원 했음</div>
                </div>
                <div className='item'>
                    <div className='username'>abcd1234</div>
                    <div className='contents'>ㅇㅇㅋㅋ</div>
                </div>
                <div className='item'>
                    <div className='username'>abcd1234</div>
                    <div className='contents'>웃겨</div>
                </div>
            </div>
        </div>
    );
};

export default RoomInfoWithMsg;