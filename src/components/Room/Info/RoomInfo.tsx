import React from 'react';
import { isLeft60Secs } from 'util/time';
import ProgressBar from '../ProgressBar/ProgressBar';
import { convertUnit } from 'util/unit';
import './RoomInfo.scss'
import { roomProps } from '../List/RoomList';

export type roomInfoProps = {
    room: roomProps;
}

const RoomInfo = (props:roomInfoProps) => {
    return (
        <>
            <div className='room_info_wrap'>
              <div className={`room_info ${isLeft60Secs(props.room.deleteDate) ? 'warning' : ''}`}>
                {
                  isLeft60Secs(props.room.deleteDate) ? 
                  <div className='banner_before_60secs'><div className='ico popped black'><span className='read_only'>펑!</span> 되기 60초 전!</div></div>
                  :
                  <>
                    <div className='count'>메시지 {convertUnit(props.room.messageCount || 0)}개</div>
                    <div className='rest'>
                      <span className='text'><span className='pop'>펑!</span>까지</span>
                      <ProgressBar deleteDate={'2024-07-04 00:00:00'} type={'dark'} />
                    </div>
                  </>
      
                }
              </div>
            </div>
        </>
    );
};

export default RoomInfo;