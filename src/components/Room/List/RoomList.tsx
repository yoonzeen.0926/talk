import React from 'react';
import './RoomList.scss'
import { useNavigate } from 'react-router-dom';

export type roomProps = {
    rank: number;
    roomId: string;
    roomName: string;
    categoryCode: string;
    messageCount: number;
    entranceCount: number;
    createCmn: string;
    deleteDate:string;
}

type roomListProps = {
    rooms: roomProps[];
    categoryCode: string;
}

const RoomList = (props:roomListProps) => {
    const temp = [
        '테스트용방 1',
        '테스트용방 2', 
        '테스트용방 3', 
        '테스트용방 4', 
        '테스트용방 5',
        '테스트용방 6',
        '테스트용방 7',
        '테스트용방 8',
        '테스트용방 9',
        '테스트용방 10',
        '테스트용방 11',
        '테스트용방 12',
        '테스트용방 13',
        '테스트용방 14',
        '테스트용방 15',
        '테스트용방 16',
        '테스트용방 17',
        '테스트용방 18',
        '테스트용방 19',
        '테스트용방 20',
        '테스트용방 21',
    ]
    const navigate = useNavigate();
    // 더보기 페이지로 이동
    const handleMore = () => {
        navigate('/talk/more');
    }
    return (
        <div className='room_list_wrap'>
            <div className='room_list_inner_wrap'>
                <ul className='room_list'>
                    {/* {
                        props.rooms.map((el:roomProps, idx:number) =>
                            <li key={el.roomId}>
                                <div className='room_name'>{el.roomName}</div>
                            </li>
                        )
                    } */}
                    <li><div className='room_name active'>테스트용방 1</div></li>
                    <li><div className='room_name'>테스트용방 2</div></li>
                    <li><div className='room_name'>테스트용방 3</div></li>
                    <li><div className='room_name'>테스트용방 4</div></li>
                    <li><div className='room_name'>테스트용방 5</div></li>
                    <li>
                        <button type="button" className={`btn more circle only_img ${temp.length <= 20 ? 'disabled' : ''}`} onClick={handleMore} disabled={temp.length <= 20}>채팅방 더보기</button>
                    </li>
                </ul>
            </div>
        </div>
    );
};

export default RoomList;